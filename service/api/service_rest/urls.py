from django.urls import path
from .views import api_technicians, api_show_technician,api_appointments,api_show_appointment, api_update_appointment_status


urlpatterns = [
    path(
        "technicians/",
        api_technicians,
        name="api_technicians",
    ),
    path(
        "technicians/<str:id>/",
        api_show_technician,
        name="api_show_technician",
    ),
    path(
        "appointments/",
        api_appointments,
        name="api_appointments",
    ),
    path(
        "appointments/<str:id>/",
        api_show_appointment,
        name="api_show_appointment",
    ),
    path(
        "appointments/<str:id>/<str:status>/",
        api_update_appointment_status,
        name="api_update_appointment_status",
    ),
]
