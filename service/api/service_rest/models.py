from django.db import models
from django.urls import reverse

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)
    def __str__(self):
        return self.vin

class Technician(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    employee_id = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return f"{self.first_name} {self.last_name}"





class Appointment(models.Model):
    date_time = models.DateTimeField(auto_now_add=True)
    reason = models.CharField(max_length=100)
    status = models.CharField(max_length=100)
    vin = models.CharField(max_length=200)
    customer = models.CharField(max_length=100)

    def get_is_vip(self):
        try:
            automobile = AutomobileVO.objects.get(vin=self.vin)
            return True
        except AutomobileVO.DoesNotExist:
            return False






    technician = models.ForeignKey(
        Technician,
        related_name="appointments",
        on_delete=models.CASCADE,
        null=True,

    )

    def get_api_url(self):
        return reverse("api_appointment", kwargs={"vin": self.vin})
