# Generated by Django 4.0.3 on 2023-07-25 21:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('service_rest', '0005_status_alter_appointment_status'),
    ]

    operations = [
        migrations.AlterField(
            model_name='appointment',
            name='status',
            field=models.CharField(max_length=100),
        ),
        migrations.DeleteModel(
            name='Status',
        ),
    ]
