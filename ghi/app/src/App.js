import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
// import Nav from "./Nav";
import TestNav from "./TestNav";
import ManufacturerList from "./ManufacturerList";
import CreateManufacturerForm from "./CreateManufacturerForm";
import ModelList from "./ModelList";
import CreateModelForm from "./CreateModelForm";
import AutomobilesList from "./AutomobilesList";
import CreateAutomobileForm from "./CreateAutomobileForm";
import SalespeopleList from "./SalespeopleList";
import AddSalesperson from "./AddSalesperson";
import CustomersList from "./CustomersList";
import AddCustomer from "./AddCustomer";
import SalesList from "./SalesList";
import AddSale from "./AddSale";
import BySalespersonList from "./BySalespersonList";
import TechniciansList from "./TechniciansList";
import AddTechnician from "./AddTechnician";
import AppointmentsList from "./AppointmentList";
import CreateServiceAppointmentForm from "./CreateServiceAppointmentForm";

function App(props) {
  return (
    <BrowserRouter>
      <TestNav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="manufacturers" element={<ManufacturerList />} />
          <Route
            path="manufacturers/new"
            element={<CreateManufacturerForm />}
          />
          <Route path="models" element={<ModelList />} />
          <Route path="models/create" element={<CreateModelForm />} />
          <Route path="automobiles" element={<AutomobilesList />} />
          <Route path="automobiles/create" element={<CreateAutomobileForm />} />
          <Route path="salespeople" element={<SalespeopleList />} />
          <Route path="salespeople/new" element={<AddSalesperson />} />
          <Route path="customers" element={<CustomersList />} />
          <Route path="customers/new" element={<AddCustomer />} />
          <Route path="sales" element={<SalesList />} />
          <Route path="sales/new" element={<AddSale />} />
          <Route path="salespersonhistory" element={<BySalespersonList />} />
          <Route path="technicians" element={<TechniciansList />} />
          <Route path="technicians/new" element={<AddTechnician />} />
          <Route
            path="appointments"
            element={<AppointmentsList filtersList={[]} />}
          />
          <Route
            path="appointments/history"
            element={<AppointmentsList filtersList={["cancel", "finish"]} />}
          />
          <Route
            path="appointments/create"
            element={<CreateServiceAppointmentForm />}
          />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
