import React, { useState, useEffect } from 'react';
import "./CustomCSS.css";


function AddCustomer() {
    const [customers, setCustomers] = useState([])
    const [first_name, setFirstName] = useState('')
    const [last_name, setLastName] = useState('')
    const [address, setAddress] = useState('')
    const [phone_number, setPhoneNumber] = useState('')

    const handleFirstNameChange = (event) => {
        setFirstName(event.target.value)
    }

    const handleLastNameChange = (event) => {
        setLastName(event.target.value)
    }

    const handleAddressChange = (event) => {
        setAddress(event.target.value)
    }

    const handlePhoneNumberChange = (event) => {
        setPhoneNumber(event.target.value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault()
        const data = {}
        data.first_name = first_name
        data.last_name = last_name
        data.address = address
        data.phone_number = phone_number
        console.log(data)

        const customerUrl = "http://localhost:8090/api/customers/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'applications/json'
            },
        }

        const response = await fetch(customerUrl, fetchConfig)
        if (response.ok) {
            const newCustomer = await response.json()
            console.log(newCustomer)
            setFirstName('')
            setLastName('')
            setAddress('')
            setPhoneNumber('')
        }
    }



    const fetchData = async () => {
    const url = "http://localhost:8090/api/customers/"
    const response = await fetch(url)

    if (response.ok) {
        const data = await response.json()
        setCustomers(data.customers)
    }
    }
    useEffect (() => {
    fetchData()
    }, [])

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4 card-color">
            <h1>Add a Customer</h1>
            <form onSubmit={handleSubmit} id="create-shoe-form">
              <div className="form-floating mb-3">
                <input
                    value={first_name}
                    onChange={handleFirstNameChange}
                    placeholder="First name"
                    required type="text"
                    name="first_name"
                    id="first_name"
                    className="form-control" />
                <label htmlFor="first_name">First name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                    value={last_name}
                    onChange={handleLastNameChange}
                    placeholder="Last Name"
                    required type="text"
                    name="last_name"
                    id="last_name"
                    className="form-control" />
                <label htmlFor="last_name">Last Name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                    value={address}
                    onChange={handleAddressChange}
                    placeholder="Address"
                    required type="text"
                    name="address"
                    id="address"
                    className="form-control" />
                <label htmlFor="address">Address</label>
                </div>
                <div className="form-floating mb-3">
                <input
                    value={phone_number}
                    onChange={handlePhoneNumberChange}
                    placeholder="Phone Number"
                    required type="text"
                    name="phone_number"
                    id="phone_number"
                    className="form-control" />
                <label htmlFor="phone_number">Phone Number</label>
                </div>
              <button className="btn create-button">Create</button>
            </form>
          </div>
        </div>
      </div>
    )
}

export default AddCustomer;
