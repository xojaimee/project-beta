import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import "./CustomCSS.css";


function CreateModelForm() {
  const [manufacturer, setManufacturer] = useState("");
  const [manufacturers, setManufacturers] = useState([]);
  const [name, setName] = useState("");
  const [pictureUrl, setPictureUrl] = useState("");

  const navigate = useNavigate();

  const createModel = async (model) => {
    const url = "http://localhost:8100/api/models/";
    const response = await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(model),
    });
    if (response.ok) {
      const data = await response.json();
      console.log(data);
      navigate("/models");
    }
  };

  const fetchManufacturers = async () => {
    const response = await fetch("http://localhost:8100/api/manufacturers/");
    if (response.ok) {
      const data = await response.json();
      setManufacturers(data.manufacturers);
    }
  };

  useEffect(() => {
    fetchManufacturers();
  }, []);

  function handleChangeManufacturer(event) {
    const value = event.target.value;
    setManufacturer(value);
  }
  function handleChangePictureUrl(event) {
    const value = event.target.value;
    setPictureUrl(value);
  }

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.name = name;
    data.manufacturer_id = parseInt(manufacturer);

    data.picture_url = pictureUrl;

    createModel(data);
  };

  const handleChangeName = (event) => {
    const value = event.target.value;
    setName(value);
  };

  return (
    <div>
      <form onSubmit={handleSubmit} id="create-model-form">
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4 card-color">
              <h1 className="card-title">Create a model!</h1>

              <div className="form-floating mb-3">
                <input
                  onChange={handleChangeName}
                  required
                  placeholder="Model Name"
                  type="text"
                  id="modelName"
                  name="modelName"
                  className="form-control"
                  value={name}
                />
                <label htmlFor="modelName">Model Name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={handleChangePictureUrl}
                  required
                  placeholder="Picture Url"
                  type="url"
                  id="modelPictureUrl"
                  name="modelPictureUrl"
                  className="form-control"
                  value={pictureUrl}
                />
                <label htmlFor="modelPictureUrl">Picture Url</label>
              </div>
              <div className="mb-3">
                <select
                  value={manufacturer}
                  onChange={handleChangeManufacturer}
                  required
                  name="manufacturer"
                  id="manufacturer"
                  className="form-select"
                >
                  <option value="">Choose a manufacturer</option>
                  {manufacturers.map((manufacturer) => {
                    return (
                      <option key={manufacturer.id} value={manufacturer.id}>
                        {manufacturer.name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-lg create-button">
                Create a model!
              </button>
            </div>
          </div>
        </div>
      </form>
    </div>
  );
}

export default CreateModelForm;
