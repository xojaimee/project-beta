import React, { useState, useEffect } from 'react';
import "./CustomCSS.css";


function AddSale() {
    const [automobiles, setAutomobiles] = useState([])
    const [salespeople, setSalespeople] = useState([])
    const [customers, setCustomers] = useState([])
    const [automobile, setAutomobile] = useState('')
    const [salesperson, setSalesperson] = useState('')
    const [customer, setCustomer] = useState('')
    const [price, setPrice] = useState('')

    const handleAutomobileChange = (event) => {
        setAutomobile(event.target.value)
    }

    const handleSalespersonChange = (event) => {
        setSalesperson(event.target.value)
    }

    const handleCustomerChange = (event) => {
        setCustomer(event.target.value)
    }

    const handlePriceChange = (event) => {
        setPrice(event.target.value)
    }
    // HANDLESUBMIT =============================

    const handleSubmit = async (event) => {
        event.preventDefault()
        const data = {}
        data.automobile = automobile
        data.salesperson = salesperson
        data.customer = customer
        data.price = price

        const salesUrl = "http://localhost:8090/api/sales/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
        }

        const response = await fetch(salesUrl, fetchConfig)
        if (response.ok) {
            const newSale = await response.json()
            console.log(newSale)
            setAutomobile('')
            setSalesperson('')
            setCustomer('')
            setPrice('')


            const automobileVIN = newSale.automobile
            const updateAutoUrl = `http://localhost:8100/api/automobiles/${automobileVIN}/`
            const updateConfig = {
                method: "put",
                body: JSON.stringify({sold:true}),
                headers: {
                    'Content-Type': 'application/json'
                },
            }
            const updateResponse = await fetch(updateAutoUrl, updateConfig)
            if (updateResponse.ok) {
                console.log('Sold status updated successfully')
            }
        }
    }

    // FETCH =====================================
    const fetchAutomobiles = async () => {
    const url = "http://localhost:8100/api/automobiles/"
    const response = await fetch(url)

    if (response.ok) {
        const data = await response.json()
        const unsoldAutomobiles = data.autos.filter((auto) => !auto.sold)
        setAutomobiles(unsoldAutomobiles)
    }
    }

    const fetchSalesperson = async () => {
    const response = await fetch("http://localhost:8090/api/salespeople/")

    if (response.ok) {
        const data = await response.json()
        setSalespeople(data.salespeople)
    }
    }

    const fetchCustomer = async () => {
    const response = await fetch("http://localhost:8090/api/customers/")

    if (response.ok) {
        const data = await response.json()
        setCustomers(data.customers)
    }
    }

    useEffect(() => {
    fetchAutomobiles()
    fetchSalesperson()
    fetchCustomer()
    }, [])


    return (
        <div className="row">
        <div className="offset-3 col-6 ">
          <div className="shadow p-4 mt-4 card-color">
            <h1>Record a new sale</h1>
            <form onSubmit={handleSubmit} id="create-shoe-form">
            <div className="mb-3">
                <select
                    value={automobile}
                    onChange={handleAutomobileChange}
                    required id="automobile"
                    name="automobile"
                    className="form-select">
                  <option value="">Select an automobile VIN</option>
                  {automobiles?.map(auto => {
                    return (
                        <option key={auto.id} value={auto.id}>
                            {auto.vin}
                        </option>
                    );
                  })
                  }
                </select>
                </div>
                <div className="mb-3">
                <select
                    value={salesperson}
                    onChange={handleSalespersonChange}
                    required id="salesperson"
                    name="salesperson"
                    className="form-select">
                  <option value="">Select a salesperson</option>
                  {salespeople?.map(salesperson => {
                    return (
                        <option key={salesperson.id} value={salesperson.id}>
                            {salesperson.first_name} {salesperson.last_name}
                        </option>
                    );
                  })
                  }
                </select>
                </div>
                <div className="mb-3">
                <select
                    value={customer}
                    onChange={handleCustomerChange}
                    required id="customer"
                    name="customer"
                    className="form-select">
                  <option value="">Select a customer</option>
                  {customers?.map(customer => {
                    return (
                        <option key={customer.id} value={customer.id}>
                            {customer.first_name} {customer.last_name}
                        </option>
                    );
                  })
                  }
                </select>
                </div>
                <div className="form-floating mb-3">
                <input
                    value={price}
                    onChange={handlePriceChange}
                    placeholder="Price"
                    required type="text"
                    name="price"
                    id="price"
                    className="form-control" />
                <label htmlFor="price">Price</label>

              </div>
              <button className="btn create-button">Create</button>
            </form>
          </div>
        </div>
      </div>
    )
}

export default AddSale;
