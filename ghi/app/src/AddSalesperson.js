import React, { useEffect, useState } from 'react';
import "./CustomCSS.css";


function AddSalesperson() {
    const [salespeople, setSalespeople] = useState([])
    const [first_name, setFirstName] = useState('')
    const [last_name, setLastName] = useState('')
    const [employee_id, setEmployeeId] = useState('')

    const handleFirstNameChange = (event) => {
        setFirstName(event.target.value)
    }

    const handleLastNameChange = (event) => {
        setLastName(event.target.value)
    }

    const handleEmployeeIdChange = (event) => {
        setEmployeeId(event.target.value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault()
        const data = {}
        data.first_name = first_name
        data.last_name = last_name
        data.employee_id = employee_id
        console.log(data)

        const salespersonUrl = "http://localhost:8090/api/salespeople/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'applications/json'
            },
        }

        const response = await fetch(salespersonUrl, fetchConfig)
        if (response.ok) {
            const newSalesperson = await response.json()
            console.log(newSalesperson)
            setFirstName('')
            setLastName('')
            setEmployeeId('')
        }
    }


    const fetchData = async () => {
    const url = "http://localhost:8090/api/salespeople/"
    const response = await fetch(url)

    if (response.ok) {
        const data = await response.json()
        setSalespeople(data.salespeople)
    }
    }
    useEffect (() => {
    fetchData()
    }, [])



    return(
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4 card-color">
            <h1>Add a Salesperson</h1>
            <form onSubmit={handleSubmit} id="create-shoe-form">
              <div className="form-floating mb-3">
                <input
                    value={first_name}
                    onChange={handleFirstNameChange}
                    placeholder="First name"
                    required type="text"
                    name="first_name"
                    id="first_name"
                    className="form-control" />
                <label htmlFor="first_name">First name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                    value={last_name}
                    onChange={handleLastNameChange}
                    placeholder="Last Name"
                    required type="text"
                    name="last_name"
                    id="last_name"
                    className="form-control" />
                <label htmlFor="last_name">Last Name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                    value={employee_id}
                    onChange={handleEmployeeIdChange}
                    placeholder="Employee ID"
                    required type="text"
                    name="employee_id"
                    id="employee_id"
                    className="form-control" />
                <label htmlFor="employee_id">Employee ID</label>
                </div>
              <button className="btn create-button">Create</button>
            </form>
          </div>
        </div>
      </div>
    )
}

export default AddSalesperson;
