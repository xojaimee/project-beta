import { NavLink } from "react-router-dom";

function TestNav() {

    return (
        <nav className="navbar navbar-expand-lg navbar-dark background-color">
        <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">
            CarCar
        </NavLink>
            <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarSupportedContent">

            <ul className="navbar-nav me-auto my-2 my-lg-0">
                <li className="nav-item dropdown">
                <a className="nav-link dropdown-toggle active" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                    Manufacturers
                </a>
                <ul className="dropdown-menu">
                <li className="nav-item">
                <NavLink className="dropdown-item" to="/manufacturers">
                Manufacturers List
                </NavLink>
            </li>
            <li className="nav-item">
                <NavLink className="dropdown-item" to="/manufacturers/new">
                Create a Manufacturer
                </NavLink>
            </li>

                </ul>
                </li>
            </ul>

            <ul className="navbar-nav me-auto my-2 my-lg-0">
                <li className="nav-item dropdown">
                <a className="nav-link dropdown-toggle active" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                    Models
                </a>
                <ul className="dropdown-menu">
                <li className="nav-item">
                <NavLink className="dropdown-item" to="/models">
                Models List
                </NavLink>
            </li>
                    <li className="nav-item">
                <NavLink
                className="dropdown-item"
                aria-current="page"
                to="/models/create"
                >
                Create a Model
                </NavLink>
            </li>
                </ul>
                </li>
            </ul>

            <ul className="navbar-nav me-auto my-2 my-lg-0">
                <li className="nav-item dropdown">
                <a className="nav-link dropdown-toggle active" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                    Automobiles
                </a>
                <ul className="dropdown-menu">
                <li className="nav-item">
                    <NavLink
                    className="dropdown-item"
                    aria-current="page"
                    to="/automobiles"
                    >
                    Automobiles List
                    </NavLink>
                </li>
                <li className="nav-item">
                    <NavLink
                    className="dropdown-item"
                    aria-current="page"
                    to="/automobiles/create"
                    >
                    Create an Automobile
                    </NavLink>
                </li>
                </ul>
                </li>
            </ul>

            <ul className="navbar-nav me-auto my-2 my-lg-0">
                <li className="nav-item dropdown">
                <a className="nav-link dropdown-toggle active" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                    Salespeople
                </a>
                <ul className="dropdown-menu">
                <li className="nav-item">
                    <NavLink
                    className="dropdown-item"
                    aria-current="page"
                    to="/salespeople"
                    >
                    Salespeople List
                    </NavLink>
                </li>
                <li className="nav-item">
                    <NavLink
                    className="dropdown-item"
                    aria-current="page"
                    to="/salespeople/new"
                    >
                    Add a Salesperson
                    </NavLink>
                </li>
                </ul>
                </li>
            </ul>


            <ul className="navbar-nav me-auto my-2 my-lg-0">
                <li className="nav-item dropdown">
                <a className="nav-link dropdown-toggle active" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                    Customers
                </a>
                <ul className="dropdown-menu">
                <li className="nav-item">
                    <NavLink
                    className="dropdown-item"
                    aria-current="page"
                    to="/customers"
                    >
                    Customers List
                    </NavLink>
                </li>
                <li className="nav-item">
                    <NavLink
                    className="dropdown-item"
                    aria-current="page"
                    to="/customers/new"
                    >
                    Add a Customer
                    </NavLink>
                </li>
                </ul>
                </li>
            </ul>


            <ul className="navbar-nav me-auto my-2 my-lg-0">
                <li className="nav-item dropdown">
                <a className="nav-link dropdown-toggle active" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                    Sales
                </a>
                <ul className="dropdown-menu">
                <li className="nav-item">
                    <NavLink className="dropdown-item" to="/sales">
                    Sales List
                    </NavLink>
                </li>
                <li className="nav-item">
                    <NavLink className="dropdown-item" to="/sales/new">
                    Add a Sale
                    </NavLink>
                </li>
                <li className="nav-item">
                    <NavLink className="dropdown-item" to="/salespersonhistory">
                    Salesperson History
                    </NavLink>
                </li>
                </ul>
                </li>
            </ul>



            <ul className="navbar-nav me-auto my-2 my-lg-0">
                <li className="nav-item dropdown">
                <a className="nav-link dropdown-toggle active" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                    Technicians
                </a>
                <ul className="dropdown-menu">
                <li className="nav-item">
                    <NavLink className="dropdown-item" to="/technicians">
                    Technicians List
                    </NavLink>
                </li>
                <li className="nav-item">
                    <NavLink className="dropdown-item" to="/technicians/new">
                    Create a Technician
                    </NavLink>
                </li>
                </ul>
                </li>
            </ul>


            <ul className="navbar-nav me-auto my-2 my-lg-0">
                <li className="nav-item dropdown">
                <a className="nav-link dropdown-toggle active" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                    Service
                </a>
                <ul className="dropdown-menu">
                <li className="nav-item">
                    <NavLink className="dropdown-item" to="/appointments">
                    Service Appointments
                    </NavLink>
                </li>
                <li className="nav-item">
                    <NavLink className="dropdown-item" to="/appointments/history">
                    Service History
                    </NavLink>
                </li>
                <li className="nav-item">
                    <NavLink className="dropdown-item" to="/appointments/create">
                    Create a Service Appointment
                    </NavLink>
                </li>
                </ul>
                </li>
            </ul>

            </div>
        </div>
        </nav>
    )
}

export default TestNav;
