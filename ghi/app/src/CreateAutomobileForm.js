import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import "./CustomCSS.css";


function CreateAutomobileForm() {
  const [color, setColor] = useState("");
  const [year, setYear] = useState("");
  const [vin, setVin] = useState("");
  const [model, setModel] = useState("");
  const [models, setModels] = useState([]);
  const navigate = useNavigate();

  const createAutomobile = async (automobile) => {
    console.log(JSON.stringify(automobile));
    const url = "http://localhost:8100/api/automobiles/";
    const response = await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(automobile),
    });
    if (response.ok) {
      const data = await response.json();

      navigate("/automobiles");
    }
  };

  const fetchModels = async () => {
    const response = await fetch("http://localhost:8100/api/models/");
    if (response.ok) {
      const data = await response.json();
      setModels(data.models);
    }
  };

  useEffect(() => {
    fetchModels();
  }, []);

  function handleChangeModel(event) {
    const value = event.target.value;
    setModel(value);
  }
  function handleChangeColor(event) {
    const value = event.target.value;
    setColor(value);
  }
  function handleChangeYear(event) {
    const value = event.target.value;
    setYear(value);
  }
  function handleChangeVin(event) {
    const value = event.target.value;
    setVin(value);
  }

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = { color, year, vin, model_id: model };

    createAutomobile(data);
  };

  return (
    <div>
      <form onSubmit={handleSubmit} id="create-automobile-form">
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4 card-color">
              <h1 className="card-title">Add an Automobile to the inventory</h1>
              <div className="form-floating mb-3">
                <input
                  onChange={handleChangeColor}
                  required
                  placeholder="Color"
                  type="text"
                  id="autoColor"
                  name="autoColor"
                  className="form-control"
                  value={color}
                />
                <label htmlFor="autoColor">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={handleChangeYear}
                  required
                  placeholder="Year"
                  type="text"
                  id="autoYear"
                  name="autoYear"
                  className="form-control"
                  value={year}
                />
                <label htmlFor="autoYear">Year</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={handleChangeVin}
                  required
                  placeholder="Vin"
                  type="text"
                  id="autoVin"
                  name="autoVin"
                  className="form-control"
                  value={vin}
                />
                <label htmlFor="autoVin">Vin</label>
              </div>
              <div className="mb-3">
                <select
                  onChange={handleChangeModel}
                  value={model}
                  required
                  name="model"
                  id="model"
                  className="form-select"
                >
                  <option value=" ">Choose a model</option>
                  {models.map((model) => {
                    return (
                      <option key={model.id} value={model.id}>
                        {model.name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-lg create-button">Create</button>
            </div>
          </div>
        </div>
      </form>
    </div>
  );
}

export default CreateAutomobileForm;
