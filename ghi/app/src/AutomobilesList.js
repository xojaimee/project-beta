import { useState, useEffect } from "react";
import "./CustomCSS.css";


function AutomobilesList() {
  const [automobiles, setAutomobiles] = useState([]);
  const fetchData = async () => {
    const response = await fetch("http://localhost:8100/api/automobiles/");
    if (response.ok) {
      const data = await response.json();
      setAutomobiles(data.autos);
      console.log(data.autos);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <table className="table table-striped table-color">
      <thead>
        <tr>
          <th>VIN</th>
          <th>Color</th>
          <th>Year</th>
          <th>Manufacturer</th>
          <th>Sold</th>
        </tr>
      </thead>
      <tbody>
        {automobiles.map((automobile) => {
          return (
            <tr key={automobile.id}>
              <td>{automobile.vin}</td>
              <td>{automobile.color}</td>
              <td>{automobile.year}</td>
              <td>{automobile.model.manufacturer}</td>
              <td>{String(automobile.sold)}</td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}

export default AutomobilesList;
